# Chat Server Task

## Summary

This is the first assignment for the module CS4400-Scalable Computing for the Master in Science of Computer Science at Trinity College Dublin.

The Chat Server implements TCP sockets to handle multiple simultaneous clients. It follows the protocol described in [Chat Server Task Description](https://www.scss.tcd.ie/Stephen.Barrett/teaching/CS4400/chat_server_task.html).

## How do I get set up

### Application Dependencies

The project is built in Java 8. It uses [Maven](https://maven.apache.org/) as project management tool.
The following dependencies are included using Maven:

* [Guice](https://github.com/google/guice) - Dependency Injection Framework - [Maven Repository](https://mvnrepository.com/artifact/com.google.inject/guice/4.0)
* [JUnit](http://junit.org/junit4/) - Unit Testing Framework - [Maven Repository](https://mvnrepository.com/artifact/junit/junit)
* [Mockito](http://site.mockito.org/) - Mocking Framework for Unit Testing - [Maven Repository](https://mvnrepository.com/artifact/org.mockito/mockito-all/1.9.5)
* [Project Lombok](https://projectlombok.org/) - Library to simplify development - [Maven Repository](https://mvnrepository.com/artifact/org.projectlombok/lombok-maven)
* [Log4j2](https://logging.apache.org/log4j/2.x/) - Logging Utility - [Maven Repository](https://mvnrepository.com/artifact/log4j/log4j)

### Environment Dependencies

* [Maven](https://maven.apache.org/). If the build and execution are done locally. More information in section [Compile and run locally](#compile-and-run-locally).

* [Docker](https://www.docker.com). If the build and execution are done by using Docker. More information in section [Compile and run with Docker](#compile-and-run-with-docker).

### How to compile and run

The project can be compiled and run in two ways, locally or by using Docker. The following sections described these alternatives.

#### Compile and run locally

In order to run the project, it has to be compiled by using Maven with the following command:

> mvn package

then, to execute it use the command:

> java -cp target/chat-server-1.0-SNAPSHOT.jar chatserver.Main [port] [ipAddres] #ipAddress is optional

The application receives the number of the port as required argument and the ipAddress as optional argument. 

The specified port will be used by the chat server socket to listen incoming connections.

If not ipAddress is given, the application will try to get the local IP of the machine. However, due to a machine has multiple network interfaces, there is not guarantee that it will give a IP address rather than 127.0.0.1 (localhost). Therefore, it is recommended to supply the IP address as argument.

Also, you can use the scripts compile.sh and start.sh for compile and run the project respectively.

The script start.sh receives the number of the port as required argument and the ipAddress as optional argument. The behavior of these arguments is the same as the command above to execute the application.

> ./start.sh [port] [ipAddress] #ipAddress is optional

#### Compile and run with Docker

In order to run the project, it has to be compiled by using Maven with the following command:

> ./compile-with-docker

then, to execute it use the command:

> ./start-with-docker.sh [port] [ipAddress] #ipAddress is optional

The application receives the number of the port as required argument and the ipAddress as optional argument.

The specified port will be used by the chat server socket to listen incoming connections.

If not ipAddress is given, the application will try to get the local IP of the machine. However, due to a machine has multiple network interfaces, there is not guarantee that it will give a IP address rather than 127.0.0.1 (localhost). Therefore, it is recommended to supply the IP address as argument.

### FAQ

**Q.** Which Maven version was used to build the project?

**A:** Maven 3.5.2

**Q.** How do I install Maven?

**A:** Please follow the steps mentioned in [Maven Installation Guide](https://maven.apache.org/install.html).

**Q.** I get error message: *Permission denied* when try to run any script.

**A:** Execute in the terminal console:

> chmod 755 start.sh compile.sh
or
> chmod 755 start-with-docker.sh compile-with-docker.sh

**Q.** What does contain the Docker image?

**A:** It contains a lightweight linux distribution called alpine. The [docker image](https://hub.docker.com/_/maven/) used contains the JDK 8 and Maven 3.5.2, which are the dependencies of the applications. Then, Maven takes care of the specific dependencies of the application mentioned in the [Application Dependencies Section](#application-dependencies).

## Who do I talk to

* Jeancarlo Arguello Calvo - arguellj@tcd.ie
* StudentID: 17307291
* Trinity College Dublin, The University of Dublin