package chatserver.server.clients;

import chatserver.readers.DataReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class AsynchronousClientTest {

    @Mock
    AsynchronousSocketChannel channelMock;

    @Mock
    DataReader readerMock;

    AsynchronousClient client;


    @Before
    public void setUp() throws Exception {
        this.client = new AsynchronousClient(this.readerMock, this.channelMock);
    }

    @Test
    public void given_openChannel_when_closeChannel_then_callCloseChannel() throws IOException {
        doReturn(true).when(this.channelMock).isOpen();
        doNothing().when(this.channelMock).close();

        this.client.close();

        verify(this.channelMock, times(1)).close();
    }

    @Test
    public void given_openChannel_when_closeChannelThrowsException_then_handleException() throws IOException {
        doReturn(true).when(this.channelMock).isOpen();
        doThrow(new IOException()).when(this.channelMock).close();

        this.client.close();

        verify(this.channelMock, times(1)).close();
    }

    @Test
    public void given_writeMessage_when_channelIsOpened_then_writeUsingChannel() throws ExecutionException, InterruptedException {
        doReturn(true).when(this.channelMock).isOpen();
        Future future = Mockito.mock(Future.class);
        doReturn(future).when(this.channelMock).write(any(ByteBuffer.class));

        this.client.writeMessage("test-message");

        verify(this.channelMock, times(1)).write(any(ByteBuffer.class));

    }

    @Test
    public void given_writeMessage_when_channelIsClose_then_neverWriteUsingChannel() throws ExecutionException, InterruptedException {
        doReturn(false).when(this.channelMock).isOpen();

        this.client.writeMessage("test-message");

        verify(this.channelMock, never()).write(any(ByteBuffer.class));

    }

    @Test
    public void given_readMessage_when_channelIsOpened_then_readUsingChannel() throws ExecutionException, InterruptedException {
        doReturn(true, false).when(this.channelMock).isOpen();
        Future future = Mockito.mock(Future.class);
        doReturn(future).when(this.channelMock).read(any(ByteBuffer.class));
        doReturn(1).when(future).get();

        this.client.read();

        verify(this.channelMock, times(1)).read(any(ByteBuffer.class));

    }

    @Test
    public void given_readMessage_when_channelIsClosed_then_neverReadUsingChannel() throws ExecutionException, InterruptedException {
        doReturn(false).when(this.channelMock).isOpen();

        this.client.read();

        verify(this.readerMock, never()).onData(any(Client.class), any(ByteBuffer.class), eq(1));

    }

}