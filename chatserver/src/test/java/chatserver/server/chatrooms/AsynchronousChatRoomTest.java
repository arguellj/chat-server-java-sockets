package chatserver.server.chatrooms;

import chatserver.server.clients.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static chatserver.Constants.TEST_CHAT_ROOM_NAME;
import static chatserver.Constants.TEST_CHAT_ROOM_REFERENCE_ID;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class AsynchronousChatRoomTest {

    @Rule
    public ExpectedException exceptionException = ExpectedException.none();

    AsynchronousChatRoom chatRoom;

    @Before
    public void setup(){
        this.chatRoom = new AsynchronousChatRoom(TEST_CHAT_ROOM_NAME, TEST_CHAT_ROOM_REFERENCE_ID);
    }

    @Test
    public void given_newChatRoom_when_chatRoomIsInitialized_then_chatRoomIsEmpty() {
        Assert.assertTrue(this.chatRoom.isEmpty());
    }

    @Test
    public void given_clientConnectedAddition_when_chatRoomIsEmpty_then_chatRoomIsNotEmpty() {
        Client clientMock = mock(Client.class);
        this.chatRoom.addClient(clientMock);

        Assert.assertFalse(this.chatRoom.isEmpty());
    }

    @Test
    public void given_clientConnectedAddedAndRemoved_when_ChatRoomIsEmpty_chatRoomIsEmpty() {
        Client clientMock = mock(Client.class);
        this.chatRoom.addClient(clientMock);
        this.chatRoom.removeClient(clientMock);

        Assert.assertTrue(this.chatRoom.isEmpty());
    }

    @Test
    public void given_nullClientAdded_when_addClient_then_throwsException() {
        exceptionException.expect(IllegalArgumentException.class);
        this.chatRoom.addClient(null);
    }

    @Test
    public void given_nullClientRemoved_when_removeClient_then_throwsException() {
        exceptionException.expect(IllegalArgumentException.class);
        this.chatRoom.removeClient(null);
    }
}