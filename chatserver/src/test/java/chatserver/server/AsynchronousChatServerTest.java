package chatserver.server;

import chatserver.server.chatrooms.ChatRoom;
import chatserver.server.chatrooms.ChatRoomFactory;
import chatserver.server.clients.Client;
import chatserver.server.clients.ClientFactory;
import chatserver.sockets.AsynchronousSocket;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.StandardSocketOptions;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static chatserver.Constants.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class AsynchronousChatServerTest {

    @Mock
    private AsynchronousSocket socketMock;

    @Mock
    private ClientFactory clientFactoryMock;

    @Mock
    private ChatRoomFactory chatRoomFactoryMock;

    private AsynchronousChatServer server;

    @Before
    public void setUp() throws Exception {
        this.server = new AsynchronousChatServer(this.socketMock, this.clientFactoryMock, this.chatRoomFactoryMock);
    }

    @Test
    public void given_invalidClientId_when_broadcastMessage_then_doNotBroadcastMessage() {
        Client client = mock(Client.class);

        this.server.broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, TEST_MESSAGE);
    }

    @Test
    public void given_invalidClientName_when_broadcastMessage_then_doNotBroadcastMessage() {
        Client client = mock(Client.class);
        doReturn(null).when(client).getName();

        this.server.broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, TEST_MESSAGE);
    }

    @Test
    public void given_invalidChatRoomReference_when_broadcastMessage_then_doNotBroadcastMessage() {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();

        this.server.broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, TEST_MESSAGE);
    }

    @Test
    public void given_validChatRoomReference_when_broadcastMessage_then_broadcastMessage() throws ExecutionException, InterruptedException {
        Client client1 = mock(Client.class);
        Client client2 = mock(Client.class);
        ChatRoom chatRoom1 = mock(ChatRoom.class);
        ChatRoom chatRoom2 = mock(ChatRoom.class);
        Set<Client> clientConnected = new HashSet<>();
        clientConnected.add(client1);
        clientConnected.add(client2);
        doReturn(TEST_CLIENT_NAME).when(client1).getName();
        doReturn(TEST_CLIENT_ID).when(client1).getJoinID();
        doReturn(clientConnected).when(chatRoom1).getClientsConnected();
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID, chatRoom1);
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID+1, chatRoom2);

        this.server.broadcastMessage(client1, TEST_CHAT_ROOM_REFERENCE_ID, TEST_MESSAGE);

        verify(chatRoom1, only()).getClientsConnected();
        verify(chatRoom2, never()).getClientsConnected();
        verify(client1, times(1)).writeMessage(TEST_MESSAGE);
        verify(client2, times(1)).writeMessage(TEST_MESSAGE);
    }

    @Test
    public void given_invalidChatRoom_when_joinToNewChatRoom_then_doNothing() {
        Client client = mock(Client.class);

        this.server.joinClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(client, never()).getChatRooms();
    }

    @Test
    public void given_validClient_when_joinToNewRoom_then_addChatRoomAndClient() {
        Client client = mock(Client.class);
        ChatRoom chatRoom = mock(ChatRoom.class);
        Set<ChatRoom> chatRooms = new HashSet<>();
        doReturn(chatRooms).when(client).getChatRooms();
        doNothing().when(chatRoom).addClient(client);
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID, chatRoom);

        this.server.joinClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(chatRoom, only()).addClient(client);
        verify(client, times(1)).getChatRooms();
        Assert.assertEquals(client.getChatRooms().size(), 1);
        Assert.assertTrue(client.getChatRooms().contains(TEST_CHAT_ROOM_REFERENCE_ID));
    }

    @Test
    public void given_validChatRoomName_when_getChatRoom_then_getChatRoom() {
        ChatRoom chatRoom = mock(ChatRoom.class);
        doReturn(TEST_CHAT_ROOM_NAME).when(chatRoom).getName();
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID, chatRoom);

        Optional<ChatRoom> response = this.server.getChatRoomByName(TEST_CHAT_ROOM_NAME);

        Assert.assertEquals(response.get(), chatRoom);
    }

    @Test
    public void given_invalidChatRoomName_when_getChatRoom_then_getChatRoom() {
        ChatRoom chatRoom = mock(ChatRoom.class);

        Optional<ChatRoom> response = this.server.getChatRoomByName(TEST_CHAT_ROOM_NAME);

        Assert.assertFalse(response.isPresent());
    }

    @Test
    public void given_newChatRoom_when_createChatRoom_then_chatRoomCreated() {
        ChatRoom chatRoom = mock(ChatRoom.class);
        doReturn(chatRoom).when(chatRoomFactoryMock).create(TEST_CHAT_ROOM_NAME, 1L);

        this.server.getOrCreateChatRoom(TEST_CHAT_ROOM_NAME);
    }

    @Test
    public void given_chatRoom_when_getChatRoom_then_chatRoomCreated() {
        ChatRoom chatRoom = mock(ChatRoom.class);
        doReturn(chatRoom).when(chatRoomFactoryMock).create(TEST_CHAT_ROOM_NAME, 1L);

        this.server.getOrCreateChatRoom(TEST_CHAT_ROOM_NAME);
    }

    @Test
    public void given_invalidClientName_when_disconnectClient_then_doNothing() {
        Client client = mock(Client.class);
        doReturn(null).when(client).getName();

        this.server.disconnectClient(client);

        verify(client, times(1)).close();
        verify(client, never()).getChatRooms();
    }

    @Test
    public void given_invalidClientId_when_disconnectClient_then_doNothing() {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(0L).when(client).getJoinID();

        this.server.disconnectClient(client);

        verify(client, times(1)).close();
        verify(client, never()).getChatRooms();
    }

    @Test
    public void given_validClientId_when_disconnectClient_then_doNothing() {
        Client client = mock(Client.class);
        ChatRoom chatRoom = mock(ChatRoom.class);
        Set<Long> chatRooms = new HashSet<>();
        chatRooms.add(TEST_CHAT_ROOM_REFERENCE_ID);
        doReturn(chatRooms).when(client).getChatRooms();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID, chatRoom);

        this.server.disconnectClient(client);

        verify(client, times(1)).close();
        verify(client, times(2)).getChatRooms();
        Assert.assertTrue(client.getChatRooms().isEmpty());
        Assert.assertFalse(client.getChatRooms().contains(TEST_CHAT_ROOM_REFERENCE_ID));
    }

    @Test
    public void given_invalidClientName_when_leaveClient_then_doNothing() {
        Client client = mock(Client.class);
        doReturn(null).when(client).getName();

        this.server.leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(client, never()).getChatRooms();
    }

    @Test
    public void given_invalidClientId_when_leaveClient_then_doNothing() {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(0L).when(client).getJoinID();

        this.server.leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(client, never()).getChatRooms();
    }

    @Test
    public void given_invalidChatRoom_when_leaveClient_then_doNothing() {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(0L).when(client).getJoinID();

        this.server.leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(client, never()).getChatRooms();
    }

    @Test
    public void given_validClientAndChatRoom_when_leaveClient_then_removeClientAndRemoveChatRoom() {
        Client client = mock(Client.class);
        ChatRoom chatRoom = mock(ChatRoom.class);
        Set<Long> chatRooms = new HashSet<>();
        chatRooms.add(TEST_CHAT_ROOM_REFERENCE_ID);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(chatRooms).when(client).getChatRooms();
        doReturn(true).when(chatRoom).removeClient(client);
        doReturn(true).when(chatRoom).isEmpty();
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID, chatRoom);

        this.server.leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(client, times(1)).getChatRooms();
        Assert.assertTrue(this.server.getChatRooms().isEmpty());
        Assert.assertTrue(client.getChatRooms().isEmpty());
    }

    @Test
    public void given_validClientAndChatRoom_when_leaveClient_then_removeClientAndKeepChatRoom() {
        Client client = mock(Client.class);
        ChatRoom chatRoom = mock(ChatRoom.class);
        Set<Long> chatRooms = new HashSet<>();
        chatRooms.add(TEST_CHAT_ROOM_REFERENCE_ID);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(chatRooms).when(client).getChatRooms();
        doReturn(true).when(chatRoom).removeClient(client);
        doReturn(false).when(chatRoom).isEmpty();
        this.server.getChatRooms().put(TEST_CHAT_ROOM_REFERENCE_ID, chatRoom);

        this.server.leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);

        verify(client, times(1)).getChatRooms();
        Assert.assertEquals(this.server.getChatRooms().size(), 1);
        Assert.assertTrue(client.getChatRooms().isEmpty());
    }

    @Test
    public void given_shutdown_when_killService_then_closeSocket() {
        doNothing().when(this.socketMock).stop();

        this.server.shutdown();

        verify(this.socketMock, only()).stop();
    }

    @Test
    public void given_newConnection_when_connectClient_then_handleConnection() throws IOException {
        AsynchronousSocketChannel channel = mock(AsynchronousSocketChannel.class);
        Client client = mock(Client.class);
        doReturn(client).when(this.clientFactoryMock).create(channel);
        doReturn(channel).when(channel).setOption(StandardSocketOptions.TCP_NODELAY, true);

        this.server.handleNewConnection(channel);
    }

    @Test
    public void given_closedConnection_when_run_then_doNotListenConnections() throws IOException {
        doNothing().when(this.socketMock).start(this.server.getIpAddress(), TEST_PORT);
        doReturn(false).when(socketMock).isOpen();
        this.server.run(TEST_PORT);

        verify(this.socketMock, never()).accept();
    }

    @Test
    public void given_closedConnection_when_run_then_ListenConnections() throws IOException {
        doNothing().when(this.socketMock).start(this.server.getIpAddress(), TEST_PORT);
        doReturn(false).when(socketMock).isOpen();
        this.server.run(TEST_PORT);

        verify(this.socketMock, never()).accept();
    }

    @Test
    public void given_newJoinId_when_requested_then_getAndIncreasedId() {
        Assert.assertEquals(0, this.server.clientIdSeed.get());
        Assert.assertEquals(1, this.server.getNextJoinId());
        Assert.assertEquals(2, this.server.getNextJoinId());
    }
}