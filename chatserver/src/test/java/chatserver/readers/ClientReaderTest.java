package chatserver.readers;

import chatserver.commands.Command;
import chatserver.commands.exceptions.CommandException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static chatserver.utils.Constants.DISCONNECT_MESSAGE;
import static chatserver.utils.Constants.HELLO_MESSAGE;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ClientReaderTest {
    private static final String DUMMY_COMMAND_IDENTIFIER = "TEST";
    private static final String UNKNOWN_COMMAND = "UNKNOWN CMD";
    private static final String COMMAND_SEPARATOR = ":";
    private static final String TESTING_MESSAGE = " testing";

    @Mock
    ChatServer serverMock;

    @Mock
    Map<String, Command> commandsMock;

    ClientReader reader;

    @Before
    public void setUp() throws Exception {
        this.reader = new ClientReader(this.serverMock, this.commandsMock);
    }

    @Test
    public void given_HeloMessage_when_getCommand_then_identifyAndExecuteProperCommand() throws CommandException {
        Client client = mock(Client.class);
        String message = HELLO_MESSAGE + ClientReaderTest.TESTING_MESSAGE;
        doReturn(false).when(this.commandsMock).containsKey(message);
        Command dummyCommand = mock(Command.class);
        doReturn(dummyCommand).when(this.commandsMock).get(HELLO_MESSAGE);
        doNothing().when(dummyCommand).execute(client, message);

        this.reader.onData(client, ByteBuffer.wrap(message.getBytes()), message.getBytes().length);

        verify(dummyCommand, only()).execute(client, message);
    }

    @Test
    public void given_validMessage_when_getCommand_then_identifyAndExecuteProperCommand() throws CommandException {
        Client client = mock(Client.class);
        String message = ClientReaderTest.DUMMY_COMMAND_IDENTIFIER + ClientReaderTest.COMMAND_SEPARATOR + ClientReaderTest.TESTING_MESSAGE;
        doReturn(true).when(this.commandsMock).containsKey(ClientReaderTest.DUMMY_COMMAND_IDENTIFIER);
        Command dummyCommand = mock(Command.class);
        doReturn(dummyCommand).when(this.commandsMock).get(ClientReaderTest.DUMMY_COMMAND_IDENTIFIER);
        doNothing().when(dummyCommand).execute(client, message);

        this.reader.onData(client, ByteBuffer.wrap(message.getBytes()), message.getBytes().length);

        verify(dummyCommand, only()).execute(client, message);
    }

    @Test
    public void given_invalidMessage_when_getCommand_then_processDisconnectCommand() throws CommandException {
        Client client = mock(Client.class);
        String message = ClientReaderTest.UNKNOWN_COMMAND + ClientReaderTest.COMMAND_SEPARATOR + ClientReaderTest.TESTING_MESSAGE;
        doReturn(false).when(this.commandsMock).containsKey(ClientReaderTest.UNKNOWN_COMMAND);
        Command dummyCommand = mock(Command.class);
        doReturn(dummyCommand).when(this.commandsMock).get(DISCONNECT_MESSAGE);
        doNothing().when(dummyCommand).process(client, null);

        this.reader.onData(client, ByteBuffer.wrap(message.getBytes()), message.getBytes().length);

        verify(dummyCommand, never()).execute(client, message);
        verify(dummyCommand, only()).process(client, null);
    }

    @Test
    public void given_validMessage_when_executeThrowsCommandException_then_handleException() throws CommandException, ExecutionException, InterruptedException {
        Client client = mock(Client.class);
        CommandException exception = mock(CommandException.class);
        String message = ClientReaderTest.DUMMY_COMMAND_IDENTIFIER + ClientReaderTest.COMMAND_SEPARATOR + ClientReaderTest.TESTING_MESSAGE;
        doReturn(true).when(this.commandsMock).containsKey(ClientReaderTest.DUMMY_COMMAND_IDENTIFIER);
        Command dummyCommand = mock(Command.class);
        doReturn(dummyCommand).when(this.commandsMock).get(ClientReaderTest.DUMMY_COMMAND_IDENTIFIER);
        doThrow(exception).when(dummyCommand).execute(client, message);
        doNothing().when(client).writeMessage(any(String.class));
        doReturn(0L).when(client).getJoinID();
        doReturn(dummyCommand).when(this.commandsMock).get(DISCONNECT_MESSAGE);
        doNothing().when(dummyCommand).process(client, null);

        this.reader.onData(client, ByteBuffer.wrap(message.getBytes()), message.getBytes().length);

        verify(dummyCommand, times(1)).execute(client, message);
        verify(dummyCommand, times(1)).process(client, null);
    }
}