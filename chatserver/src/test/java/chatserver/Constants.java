package chatserver;

public class Constants {
    public static final String TEST_CHAT_ROOM_NAME = "test-room";
    public static final long TEST_CHAT_ROOM_REFERENCE_ID = 1L;
    public static final String TEST_MESSAGE = "test message";
    public static final String TEST_CLIENT_NAME = "client test";
    public static final long TEST_CLIENT_ID = 5;
    public static final int TEST_PORT = 4000;
    public static final String TEST_HOSTNAME = "10.2.10.10";
    public static final int TEST_STUDENT_ID = 17307291;
}
