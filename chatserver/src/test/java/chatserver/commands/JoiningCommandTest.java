package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.server.ChatServer;
import chatserver.server.chatrooms.ChatRoom;
import chatserver.server.clients.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static chatserver.Constants.*;
import static chatserver.utils.Constants.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class JoiningCommandTest {
    @Rule
    public ExpectedException exceptionException = ExpectedException.none();

    @Mock
    private ChatServer chatServerMock;

    private JoiningCommand command;

    @Before
    public void setUp() throws Exception {
        this.command = new JoiningCommand(this.chatServerMock);
    }

    @Test
    public void given_commandPattern_when_get_then_returnCorrectPattern() {
        String expected = "(" + JOIN_CHAT_ROOM_MESSAGE + STRING_COMMAND_PARAMETER_PATTERN + ")"
                + "(" + CLIENT_IP_MESSAGE + STRING_COMMAND_PARAMETER_PATTERN + ")"
                + "(" + PORT_MESSAGE + NUMERIC_COMMAND_PARAMETER_PATTERN + ")"
                + "(" + CLIENT_NAME_MESSAGE + STRING_COMMAND_PARAMETER_PATTERN + ")";
        String pattern = this.command.buildCommandPattern();

        Assert.assertEquals(expected, pattern);
    }

    @Test
    public void given_responseFormat_when_get_returnCorrectFormat() {
        String expected = JOINED_CHAT_ROOM_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator")
                + SERVER_IP_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator")
                + PORT_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator")
                + ROOM_REFERENCE_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator")
                + JOIN_ID_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator");
        String format = this.command.buildResponseFormat();

        Assert.assertEquals(expected, format);
    }

    @Test
    public void given_validClient_when_validate_then_noException() throws CommandException {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add("dummy");
        params.add(TEST_CLIENT_NAME);
        this.command.validate(client, params);
    }

    @Test
    public void given_invalidClientName_when_validate_then_setName() throws CommandException {
        Client client = mock(Client.class);
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add("dummy");
        params.add(TEST_CLIENT_NAME);
        doCallRealMethod().when(client).setName(TEST_CLIENT_NAME);
        this.command.validate(client, params);

        verify(client, times(1)).setName(TEST_CLIENT_NAME);
    }

    @Test
    public void given_invalidClientIndex_when_validate_then_throwException() throws CommandException {
        exceptionException.expect(InvalidClientNameException.class);
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add("dummy");
        params.add("dummy");
        this.command.validate(client, params);
    }

    @Test
    public void given_processCommand_when_callProcess_then_processCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String message = "CHAT:1\nCLIENT_NAME:client test\nMESSAGE:'client test' has joined to chatroom 'test-room'.\n\n";
        String response = "JOINED_CHATROOM:test-room\n" +
                "SERVER_IP:10.2.10.10\n" +
                "PORT:4000\n" +
                "ROOM_REF:1\n" +
                "JOIN_ID:5\n";
        ArrayList<String> params = new ArrayList<>();
        params.add(TEST_CHAT_ROOM_NAME);

        Client client = mock(Client.class);
        ChatRoom chatRoom = mock(ChatRoom.class);
        doReturn(chatRoom).when(this.chatServerMock).getOrCreateChatRoom(TEST_CHAT_ROOM_NAME);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_HOSTNAME).when(this.chatServerMock).getIpAddress();
        doReturn(TEST_PORT).when(this.chatServerMock).getPort();
        doReturn(TEST_CHAT_ROOM_REFERENCE_ID).when(chatRoom).getReferenceId();
        doNothing().when(this.chatServerMock).joinClient(client, TEST_CHAT_ROOM_REFERENCE_ID);
        doNothing().when(client).writeMessage(response);


        this.command.process(client, params);

        verify(this.chatServerMock, times(1)).joinClient(client, TEST_CHAT_ROOM_REFERENCE_ID);
        verify(this.chatServerMock, times(1)).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, message);
    }

    @Test
    public void given_executeCommand_when_execute_then_executeCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String message = "CHAT:1\nCLIENT_NAME:client test\nMESSAGE:'client test' has joined to chatroom 'test-room'.\n\n";
        String response = "JOINED_CHATROOM:test-room\n" +
                "SERVER_IP:10.2.10.10\n" +
                "PORT:4000\n" +
                "ROOM_REF:1\n" +
                "JOIN_ID:5\n";
        String messageExecuted = "JOIN_CHATROOM:" + TEST_CHAT_ROOM_NAME +"\n" +
                "CLIENT_IP: \n" +
                "PORT: \n" +
                "CLIENT_NAME:"+ TEST_CLIENT_NAME + "\n";
        ArrayList<String> params = new ArrayList<>();
        params.add(TEST_CHAT_ROOM_NAME);

        Client client = mock(Client.class);
        ChatRoom chatRoom = mock(ChatRoom.class);
        doReturn(chatRoom).when(this.chatServerMock).getOrCreateChatRoom(TEST_CHAT_ROOM_NAME);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_HOSTNAME).when(this.chatServerMock).getIpAddress();
        doReturn(TEST_PORT).when(this.chatServerMock).getPort();
        doReturn(TEST_CHAT_ROOM_REFERENCE_ID).when(chatRoom).getReferenceId();
        doNothing().when(this.chatServerMock).joinClient(client, TEST_CHAT_ROOM_REFERENCE_ID);
        doNothing().when(client).writeMessage(response);


        this.command.execute(client, messageExecuted);

        verify(this.chatServerMock, times(1)).joinClient(client, TEST_CHAT_ROOM_REFERENCE_ID);
        verify(this.chatServerMock, times(1)).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, message);
    }
}