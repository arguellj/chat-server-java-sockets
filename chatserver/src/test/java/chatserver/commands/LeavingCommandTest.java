package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.commands.exceptions.InvalidJoinIdException;
import chatserver.commands.exceptions.NotRegisteredClientException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static chatserver.Constants.*;
import static chatserver.utils.Constants.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class LeavingCommandTest {
    @Rule
    public ExpectedException exceptionException = ExpectedException.none();

    @Mock
    private ChatServer chatServerMock;

    private LeavingCommand command;

    @Before
    public void setUp() throws Exception {
        this.command = new LeavingCommand(this.chatServerMock);
    }

    @Test
    public void given_commandPattern_when_get_then_returnCorrectPattern() {
        String expected = "(" + LEAVE_CHAT_ROOM_MESSAGE + NUMERIC_COMMAND_PARAMETER_PATTERN + ")"
                + "(" + JOIN_ID_MESSAGE + NUMERIC_COMMAND_PARAMETER_PATTERN + ")"
                + "(" + CLIENT_NAME_MESSAGE + STRING_COMMAND_PARAMETER_PATTERN + ")";
        String pattern = this.command.buildCommandPattern();

        Assert.assertEquals(expected, pattern);
    }

    @Test
    public void given_responseFormat_when_get_returnCorrectFormat() {
        String expected = LEFT_CHAT_ROOM_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator")
                + JOIN_ID_MESSAGE + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator");
        String format = this.command.buildResponseFormat();

        Assert.assertEquals(expected, format);
    }

    @Test
    public void given_validClient_when_validate_then_noException() throws CommandException {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add(String.valueOf(TEST_CLIENT_ID));
        params.add(TEST_CLIENT_NAME);
        this.command.validate(client, params);
    }

    @Test
    public void given_invalidClientName_when_validate_then_throwException() throws CommandException {
        exceptionException.expect(NotRegisteredClientException.class);
        Client client = mock(Client.class);
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add(String.valueOf(TEST_CLIENT_ID));
        params.add(TEST_CLIENT_NAME);
        this.command.validate(client, params);

        verify(client, times(1)).setName(TEST_CLIENT_NAME);
    }

    @Test
    public void given_invalidClientIndex_when_validate_then_throwException() throws CommandException {
        exceptionException.expect(InvalidClientNameException.class);
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add("dummy");
        this.command.validate(client, params);
    }

    @Test
    public void given_invalidClientID_when_validate_then_throwException() throws CommandException {
        exceptionException.expect(InvalidJoinIdException.class);
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("10");
        params.add(TEST_CLIENT_NAME);
        this.command.validate(client, params);
    }

    @Test
    public void given_processCommand_when_callProcess_then_processCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String broadcast = "CHAT:1\n" +
                "CLIENT_NAME:client test\n" +
                "MESSAGE:'client test' has left the chatroom '1'.\n\n";
        String response = "LEFT_CHATROOM:1\n" +
                "JOIN_ID:5\n";

        Client client = mock(Client.class);
        ArrayList<String> params = new ArrayList<>();
        params.add(String.valueOf(TEST_CHAT_ROOM_REFERENCE_ID));
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doNothing().when(client).writeMessage(response);

        this.command.process(client, params);

        verify(this.chatServerMock, times(1)).leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);
        verify(this.chatServerMock, times(1)).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, broadcast);
    }

    @Test
    public void given_executeCommand_when_execute_then_executeCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String message = "LEAVE_CHATROOM: " + TEST_CHAT_ROOM_REFERENCE_ID + "\n" +
                "JOIN_ID: " + TEST_CLIENT_ID + "\n" +
                "CLIENT_NAME: " + TEST_CLIENT_NAME + "\n";
        String broadcast = "CHAT:1\n" +
                "CLIENT_NAME:client test\n" +
                "MESSAGE:'client test' has left the chatroom '1'.\n\n";
        String response = "LEFT_CHATROOM:1\n" +
                "JOIN_ID:5\n";

        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        doNothing().when(client).writeMessage(response);

        this.command.execute(client, message);

        verify(this.chatServerMock, times(1)).leaveClient(client, TEST_CHAT_ROOM_REFERENCE_ID);
        verify(this.chatServerMock, times(1)).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, broadcast);
    }
}