package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.commands.exceptions.NotRegisteredClientException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;

import static chatserver.Constants.*;
import static chatserver.utils.Constants.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class DisconnectCommandTest {

    @Rule
    public ExpectedException exceptionException = ExpectedException.none();

    @Mock
    private ChatServer chatServerMock;

    private DisconnectCommand command;

    @Before
    public void setUp() throws Exception {
        this.command = new DisconnectCommand(this.chatServerMock);
    }

    @Test
    public void given_commandPattern_when_get_then_returnCorrectPattern() {
        String expected = "(" + DISCONNECT_MESSAGE + STRING_COMMAND_PARAMETER_PATTERN + ")"
                + "(" + PORT_MESSAGE + NUMERIC_COMMAND_PARAMETER_PATTERN + ")"
                +"(" + CLIENT_NAME_MESSAGE + STRING_COMMAND_PARAMETER_PATTERN + ")";
        String pattern = this.command.buildCommandPattern();

        Assert.assertEquals(expected, pattern);
    }

    @Test
    public void given_responseFormat_when_get_returnCorrectFormat() {
        String expected = null;
        String format = this.command.buildResponseFormat();

        Assert.assertEquals(expected, format);
    }

    @Test
    public void given_validClient_when_validate_then_noException() throws CommandException {
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add(TEST_CLIENT_NAME);
        this.command.validate(client, params);
    }

    @Test
    public void given_invalidClientName_when_validate_then_throwException() throws CommandException {
        exceptionException.expect(NotRegisteredClientException.class);
        Client client = mock(Client.class);
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add(TEST_CLIENT_NAME);
        this.command.validate(client, params);
    }

    @Test
    public void given_invalidClientIndex_when_validate_then_throwException() throws CommandException {
        exceptionException.expect(InvalidClientNameException    .class);
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        final ArrayList<String> params = new ArrayList<>();
        params.add("dummy");
        params.add("dummy");
        params.add("dummy");
        this.command.validate(client, params);
    }

    @Test
    public void given_processCommand_when_callProcess_then_processCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String message = "CHAT:1\nCLIENT_NAME:client test\nMESSAGE:'client test' has left the chatroom '1.\n\n";

        Client client = mock(Client.class);
        HashSet<Long> chatRooms = new HashSet<>();
        chatRooms.add(TEST_CHAT_ROOM_REFERENCE_ID);
        doReturn(chatRooms).when(client).getChatRooms();
        doNothing().when(this.chatServerMock).disconnectClient(client);
        doNothing().when(this.chatServerMock).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, message);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        ArrayList<String> params = new ArrayList<>();
        params.add(message);

        this.command.process(client, params);

        verify(this.chatServerMock, times(1)).disconnectClient(client);
        verify(this.chatServerMock, times(1)).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, message);
    }

    @Test
    public void given_executeCommand_when_execute_then_executeCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String broadcastMessage = "CHAT:1\nCLIENT_NAME:client test\nMESSAGE:'client test' has left the chatroom '1.\n\n";
        String message = DISCONNECT_MESSAGE + COMMAND_PARAMETER_SEPARATOR + TEST_CHAT_ROOM_NAME + System.lineSeparator()
                + PORT_MESSAGE + COMMAND_PARAMETER_SEPARATOR + TEST_PORT + System.lineSeparator()
                + CLIENT_NAME_MESSAGE + COMMAND_PARAMETER_SEPARATOR + TEST_CLIENT_NAME + System.lineSeparator();
        Client client = mock(Client.class);
        HashSet<Long> chatRooms = new HashSet<>();
        chatRooms.add(TEST_CHAT_ROOM_REFERENCE_ID);
        doReturn(chatRooms).when(client).getChatRooms();
        doNothing().when(this.chatServerMock).disconnectClient(client);
        doNothing().when(this.chatServerMock).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, broadcastMessage);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_CLIENT_NAME).when(client).getName();
        ArrayList<String> params = new ArrayList<>();
        params.add(message);

        this.command.execute(client, message);

        verify(this.chatServerMock, times(1)).disconnectClient(client);
        verify(this.chatServerMock, times(1)).broadcastMessage(client, TEST_CHAT_ROOM_REFERENCE_ID, broadcastMessage);
    }
}