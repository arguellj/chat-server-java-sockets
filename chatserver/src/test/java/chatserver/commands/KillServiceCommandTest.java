package chatserver.commands;

import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static chatserver.utils.Constants.KILL_MESSAGE;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class KillServiceCommandTest {
    @Mock
    ChatServer serverMock;

    KillServiceCommand command;

    @Before
    public void setUp() throws Exception {
        this.command = new KillServiceCommand(this.serverMock);
    }

    @Test
    public void given_validMessage_when_executeCommand_shutdownServer() throws Exception {
        Client client = mock(Client.class);
        doNothing().when(this.serverMock).shutdown();

        this.command.execute(client, KILL_MESSAGE);

        verify(serverMock, only()).shutdown();
    }

}