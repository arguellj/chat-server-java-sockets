package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static chatserver.Constants.*;
import static chatserver.utils.Constants.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class GreetingCommandTest {
    @Mock
    private ChatServer chatServerMock;

    private GreetingCommand command;

    @Before
    public void setUp() throws Exception {
        this.command = new GreetingCommand(this.chatServerMock);
    }

    @Test
    public void given_commandPattern_when_get_then_returnCorrectPattern() {
        String expected = "(" + HELLO_MESSAGE + HELO_COMMAND_PARAMETER_PATTERN + ")";
        String pattern = this.command.buildCommandPattern();

        Assert.assertEquals(expected, pattern);
    }

    @Test
    public void given_responseFormat_when_get_returnCorrectFormat() {
        String expected = SERVER_METADATA;
        String format = this.command.buildResponseFormat();

        Assert.assertEquals(expected, format);
    }

    @Test
    public void given_validClient_when_validate_then_noException() {
        Client client = mock(Client.class);
        this.command.validate(client, new ArrayList<>());

        verifyZeroInteractions(client);
    }

    @Test
    public void given_processCommand_when_callProcess_then_processCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String message = HELLO_MESSAGE + " testing";
        String response = String.format(SERVER_METADATA, message, TEST_HOSTNAME, TEST_PORT, TEST_STUDENT_ID);
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_PORT).when(this.chatServerMock).getPort();
        doReturn(TEST_HOSTNAME).when(this.chatServerMock).getIpAddress();
        doReturn(TEST_STUDENT_ID).when(this.chatServerMock).getStudentId();
        doNothing().when(client).writeMessage(response);
        ArrayList<String> params = new ArrayList<>();
        params.add(message);

        this.command.process(client, params);

        verify(client, times(1)).writeMessage(response);
    }

    @Test
    public void given_executeCommand_when_execute_then_executeCorrectlyCommand() throws CommandException, ExecutionException, InterruptedException {
        String message = HELLO_MESSAGE + " testing" + System.lineSeparator();
        String response = String.format(SERVER_METADATA, message.trim(), TEST_HOSTNAME, TEST_PORT, TEST_STUDENT_ID);
        Client client = mock(Client.class);
        doReturn(TEST_CLIENT_ID).when(client).getJoinID();
        doReturn(TEST_PORT).when(this.chatServerMock).getPort();
        doReturn(TEST_HOSTNAME).when(this.chatServerMock).getIpAddress();
        doReturn(TEST_STUDENT_ID).when(this.chatServerMock).getStudentId();
        doNothing().when(client).writeMessage(response);

        this.command.execute(client, message);

        verify(client, times(1)).writeMessage(response);
    }
}