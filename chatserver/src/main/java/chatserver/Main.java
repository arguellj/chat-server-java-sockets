package chatserver;

import chatserver.server.ChatServer;
import com.google.inject.Guice;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class Main {
    public static final int PORT_ARGUMENT_INDEX = 0;
    public static final int IP_ADDRESS_ARGUMENT_INDEX = 1;

    public static void main(String[] args) throws Exception {
        int port = 0;
        String ipAddress = null;

        if (args.length >= PORT_ARGUMENT_INDEX + 1) {
            try {
                port = Integer.parseInt(args[PORT_ARGUMENT_INDEX]);
            } catch (NumberFormatException e) {
                log.error(String.format("Argument port %s must be an integer.", args[PORT_ARGUMENT_INDEX]));
                System.exit(1);
            }
        } else {
            log.error("Invalid number of arguments. Expected at least one argument");
            System.exit(1);
        }

        if (args.length == IP_ADDRESS_ARGUMENT_INDEX + 1) {
            ipAddress = args[IP_ADDRESS_ARGUMENT_INDEX];
        }

        // Gets the chat server as singleton.
        ChatServer chatServerInstance = Guice.createInjector(new ChatServerModule()).getInstance(ChatServer.class);
        if (ipAddress != null) {
            // Use the ip address if it is provided.
            chatServerInstance.setIpAddress(ipAddress);
        }
        chatServerInstance.run(port);
    }
}