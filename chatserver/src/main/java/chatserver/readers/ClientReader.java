package chatserver.readers;

import chatserver.commands.Command;
import chatserver.commands.ErrorCommand;
import chatserver.commands.exceptions.CommandException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import java.nio.ByteBuffer;
import java.util.Map;

import static chatserver.utils.Constants.*;

@Log4j2
public class ClientReader extends DataReader {

    @Inject
    public ClientReader(ChatServer chatServer, Map<String, Command> commands) {
        super(chatServer, commands);
    }

    /**
     * Append the read buffer to the clients message buffer
     * @param client the asynchronousClient to append messages to
     * @param buffer the buffer we received from the socket
     * @param bytes the number of bytes read into the buffer
     */
    @Override
    public void onData(@NonNull final Client client, @NonNull final ByteBuffer buffer, int bytes) {
        buffer.flip();
        String message = new String(buffer.array(), 0, bytes);
        log.debug(String.format("Message received '%s'", message));

        final int commandParameterSeparatorPosition = message.indexOf(COMMAND_PARAMETER_SEPARATOR);
        String commandIdentifier = commandParameterSeparatorPosition == -1 ?
                message.trim() : message.substring(0, commandParameterSeparatorPosition);

        if (!this.commands.containsKey(commandIdentifier)) {
            if (message.startsWith(HELLO_MESSAGE)) {
                commandIdentifier = HELLO_MESSAGE;
            } else {
                log.warn(String.format("Unknown message '%s' received from Client with id '%s'.", message, client.getJoinID()));
                this.sendDisconnectMessage(client);
                return;
            }
        }

        Command command = this.commands.get(commandIdentifier);
        try {
            command.execute(client, message);
        } catch (CommandException e) {
            log.warn(e);
            ErrorCommand.handle(e, client);
            this.sendDisconnectMessage(client);
        }
    }

    private void sendDisconnectMessage(Client client) {
        try {
            this.commands.get(DISCONNECT_MESSAGE).process(client, null);
        } catch (CommandException e) {
            log.warn(e);
        }
    }
}