package chatserver.readers;

import chatserver.commands.Command;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;

import java.nio.ByteBuffer;
import java.util.Map;

/**
 * Reader used to process a client's message. It consists of commands that are available in the chat server.
 */
public abstract class DataReader {

    protected Map<String, Command> commands;

    protected final ChatServer chatServer;

    public DataReader(final ChatServer chatServer, Map<String, Command> commands) {
        this.chatServer = chatServer;
        this.commands = commands;
    }

    /**
     * Event to process the message that comes in the {@param buffer} with size {@param bytes}.
     * @param client the client who sent the message
     * @param buffer the buffer recovered
     * @param bytes the size of the chanel in bytes
     */
    public abstract void onData(final Client client, final ByteBuffer buffer, final int bytes);
}