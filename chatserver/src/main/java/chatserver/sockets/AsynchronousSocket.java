package chatserver.sockets;

import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

public interface AsynchronousSocket extends Socket {

    Future<AsynchronousSocketChannel> accept();
}
