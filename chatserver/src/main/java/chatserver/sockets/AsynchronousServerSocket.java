package chatserver.sockets;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Log4j2
public class AsynchronousServerSocket implements AsynchronousSocket {
    private final AsynchronousChannelGroup channels;
    private AsynchronousServerSocketChannel socket;

    public AsynchronousServerSocket() throws IOException {
        this.channels = AsynchronousChannelGroup.withFixedThreadPool(
                Runtime.getRuntime().availableProcessors(),
                Executors.defaultThreadFactory());
    }

    @Override
    public Future<AsynchronousSocketChannel> accept() {
        return socket.accept();
    }

    @Override
    public synchronized boolean isOpen() {
        return this.socket.isOpen();
    }

    @Override
    public void start(final String hostName, final int port) throws IOException {
        this.socket = AsynchronousServerSocketChannel.open(this.channels);
        this.socket.setOption(StandardSocketOptions.SO_REUSEADDR, true);
        InetSocketAddress socketAddress = new InetSocketAddress(hostName, port);
        this.socket.bind(socketAddress);

        log.info(String.format("Server is listening at %s%n", socketAddress));
    }

    @Override
    public synchronized void stop() {
        try {
            this.channels.shutdownNow();
            this.channels.awaitTermination(1, TimeUnit.SECONDS);
            this.socket.close();
        } catch (InterruptedException | IOException e) {
            log.error(String.format("The server could not be shutdown.", e));
        }
    }
}