package chatserver.sockets;

import java.io.IOException;

public interface Socket {

    boolean isOpen();

    void start(final String hostName, final int port) throws IOException;

    void stop();
}
