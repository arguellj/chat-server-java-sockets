package chatserver.server;

import chatserver.server.chatrooms.ChatRoom;
import chatserver.server.chatrooms.ChatRoomFactory;
import chatserver.server.clients.Client;
import chatserver.server.clients.ClientFactory;
import chatserver.sockets.Socket;
import com.google.inject.Inject;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static chatserver.utils.Constants.*;
import static java.net.InetAddress.getLocalHost;

/**
 * The chat server who manages everything.
 */
public abstract class ChatServer {

    protected ChatRoomFactory chatRoomFactory;

    @Getter
    protected Map<Long, ChatRoom> chatRooms;

    protected ClientFactory clientFactory;

    @Getter
    @Setter
    protected String ipAddress;

    @Getter
    protected int port;

    @Getter
    protected int studentId;

    protected AtomicLong chatRoomIdSeed;

    protected AtomicLong clientIdSeed;

    protected AtomicReference<Socket> socket;

    @Inject
    protected ChatServer(final Socket socket, final ClientFactory clientFactory, final ChatRoomFactory chatRoomFactory) throws UnknownHostException {
        this.ipAddress = getLocalHost().getHostAddress();
        this.studentId = STUDENT_ID;
        this.socket = new AtomicReference<>(socket);
        this.chatRoomIdSeed = new AtomicLong(CHAT_ROOM_REFERENCE_SEED_INITIAL_VALUE);
        this.clientIdSeed = new AtomicLong(CLIENT_JOIN_ID_SEED_INITIAL_VALUE);
        this.clientFactory = clientFactory;
        this.chatRoomFactory = chatRoomFactory;
    }

    /**
     * Gets the next join ID for a client. It is thread-safe.
     * @return next joint id
     */
    public long getNextJoinId() {
        return this.clientIdSeed.incrementAndGet();
    }

    /**
     * Sends the {@param message} to all the client in {@param chatRoomReference}.
     * @param client the client who send the messages
     * @param chatRoomReference the chat room reference where the message will be sent
     * @param message the message the will be sent
     */
    public abstract void broadcastMessage(@NonNull final Client client, @NonNull final long chatRoomReference, @NonNull final String message);

    /**
     * Gets or create a chat room with name {@param chatRoomName}
     * @param chatRoomName
     * @return the chat room gotten or created
     */
    public abstract ChatRoom getOrCreateChatRoom(@NonNull String chatRoomName);

    /**
     * Disconnect the {@param client}. This client leaves all the chat rooms and then its socket channel is closed.
     * @param client the client to be disconnected.
     */
    public abstract void disconnectClient(@NonNull final Client client);

    /**
     * Joins the {@param client} to the chat room {@param chatRoomReference}.
     * @param client the client to joined
     * @param chatRoomReference the chat room reference where the client will be added.
     */
    public abstract void joinClient(@NonNull final Client client, @NonNull final long chatRoomReference);

    /**
     * Removes the {@param client} from the chat room {@param chatRoomReference}.
     * @param client the client who will leave the chat room
     * @param chatRoomReference the chat room reference to be left
     */
    public abstract void leaveClient(@NonNull final Client client, @NonNull final long chatRoomReference);

    /**
     * Gets the chat room with name {@param chatRoomName}.
     * @param chatRoomName the name of the chat room
     * @return an optional Chat room
     */
    public abstract Optional<ChatRoom> getChatRoomByName(@NonNull final String chatRoomName);

    /**
     * Shuts down the chat server. It closes all the socket channels.
     */
    public abstract void shutdown();

    /**
     * Starts the chat server's execution.
     * @param port the port to be listen
     * @throws IOException
     */
    public abstract void run(int port) throws IOException;
}
