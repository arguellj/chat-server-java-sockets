package chatserver.server;

import chatserver.server.chatrooms.ChatRoom;
import chatserver.server.chatrooms.ChatRoomFactory;
import chatserver.server.clients.Client;
import chatserver.server.clients.ClientFactory;
import chatserver.sockets.AsynchronousSocket;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.StandardSocketOptions;
import java.net.UnknownHostException;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import static chatserver.utils.Constants.CLIENT_JOIN_ID_SEED_INITIAL_VALUE;

@Log4j2
@Singleton
public final class AsynchronousChatServer extends ChatServer {

    @Inject
    public AsynchronousChatServer(final AsynchronousSocket socket, final ClientFactory clientFactory, final ChatRoomFactory chatRoomFactory) throws UnknownHostException {
        super(socket, clientFactory, chatRoomFactory);
        this.chatRooms = new ConcurrentHashMap<>();
    }

    @Override
    public void broadcastMessage(@NonNull final Client client, final long chatRoomReference, @NonNull final String message) {
        // if client does not have either name or join id is because it has not joined to any chat room.
        if (client.getName() == null || client.getJoinID() == CLIENT_JOIN_ID_SEED_INITIAL_VALUE) {
            log.debug("Client not registered tried to broadcast message.");
            return;
        }

        if (!this.chatRooms.containsKey(chatRoomReference)) {
            // TODO: throws error because chat room does not exist?
            log.debug("Message sent to non existing chat room.");
            return;
        }

        this.chatRooms.get(chatRoomReference).getClientsConnected()
                .parallelStream()
                .forEach(clientConnected -> {
                    try {
                        clientConnected.writeMessage(message);
                    } catch (ExecutionException | InterruptedException e) {
                        log.warn(String.format("Could not write to client id '%d'.", client.getJoinID()), e);
                        this.disconnectClient(clientConnected);
                    }
                });
    }

    @Override
    public ChatRoom getOrCreateChatRoom(@NonNull String chatRoomName) {
        Optional<ChatRoom> chatRoom = this.getChatRoomByName(chatRoomName);

        if (chatRoom.isPresent()) {
            return chatRoom.get();
        }

        ChatRoom newChatRoom = this.chatRoomFactory.create(chatRoomName, this.chatRoomIdSeed.incrementAndGet());
        this.chatRooms.put(newChatRoom.getReferenceId(), newChatRoom);

        return newChatRoom;
    }

    @Override
    public void disconnectClient(@NonNull final Client client) {
        // if client does not have either name or join id is because it has not joined to any chat room.
        if (client.getName() != null && client.getJoinID() != 0) {
            client.getChatRooms()
                    .parallelStream()
                    .forEach((Long chatRoomReference) -> {
                        this.chatRooms.get(chatRoomReference).removeClient(client);

                        // Remove chat room if there is not clients connected to it.
                        if (this.chatRooms.get(chatRoomReference).isEmpty()) {
                            this.chatRooms.remove(chatRoomReference);
                        }
                    });
            client.getChatRooms().clear();
        }

        client.close();
    }

    @Override
    public void joinClient(@NonNull final Client client, final long chatRoomReference) {
        final ChatRoom chatRoom = this.chatRooms.get(chatRoomReference);

        if (chatRoom == null) {
            return;
        }

        chatRoom.addClient(client);
        client.getChatRooms().add(chatRoomReference);
    }

    @Override
    public void leaveClient(@NonNull final Client client, final long chatRoomReference) {
        // if client does not have either name or join id is because it has not joined to any chat room.
        if (client.getName() == null || client.getJoinID() == CLIENT_JOIN_ID_SEED_INITIAL_VALUE) {
            return;
        }

        if (!this.chatRooms.containsKey(chatRoomReference)) {
            return;
        }

        this.chatRooms.get(chatRoomReference).removeClient(client);

        // Remove chat room if there is not clients connected to it.
        if (this.chatRooms.get(chatRoomReference).isEmpty()) {
            this.chatRooms.remove(chatRoomReference);
        }

        client.getChatRooms().remove(chatRoomReference);
    }

    @Override
    public void run(int port) {
        this.port = port;

        try {
            this.socket.get().start(this.ipAddress, port);
        } catch (IOException e) {
            log.error(String.format("Socket could not start in IP: '%s' Port '%d'", this.ipAddress, port), e);
        }

        while (this.socket.get().isOpen()) {
            try {
                final AsynchronousSocketChannel result = ((AsynchronousSocket) this.socket.get()).accept().get();
                CompletableFuture.runAsync(() -> handleNewConnection(result));
            } catch (InterruptedException | ExecutionException e) {
                log.warn("Server socket was disconnecting while listening.", e);
                return;
            }
        }
    }

    @Override
    public void shutdown() {
        this.socket.get().stop();
    }

    @Override
    public Optional<ChatRoom> getChatRoomByName(@NonNull String chatRoomName) {
        return this.chatRooms.values()
                .parallelStream()
                .filter((ChatRoom chatRoom) -> chatRoomName.equals(chatRoom.getName()))
                .findFirst();
    }

    /**
     * Handles a new client' connection. Create the client and associates the current channel. Finally, start listening
     * messages from this new client.
     * @param channel the client's channel
     */
    void handleNewConnection(final AsynchronousSocketChannel channel) {
        Client client = this.clientFactory.create(channel);

        try {
            channel.setOption(StandardSocketOptions.TCP_NODELAY, true);
        } catch (IOException e) {
            log.warn(String.format("Options for client's socket with id '%d' failed to be set.", client.getJoinID()));
        }

        CompletableFuture.runAsync(() -> {
            try {
                client.read();
            } catch (ExecutionException | InterruptedException e) {
                log.warn(String.format("Unable to read client's channel with id '%d'.", client.getJoinID()));
                this.disconnectClient(client);
            }
        });

        try {
            log.info(String.format("Client connected at '%s'", channel.getRemoteAddress()));
        } catch (IOException e) {
            log.warn(String.format("Client's address with id '%d could not be gotten.", client.getJoinID()), e);
        }
    }
}
