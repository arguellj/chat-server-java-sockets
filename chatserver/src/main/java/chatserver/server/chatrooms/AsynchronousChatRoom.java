package chatserver.server.chatrooms;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Concrete {@link ChatRoom} for Asynchronous environments.
 */
public final class AsynchronousChatRoom extends ChatRoom {

    @Inject
    public AsynchronousChatRoom(@Assisted String name, @Assisted long referenceId) {
        super(name, referenceId);

        this.clientsConnected = ConcurrentHashMap.newKeySet();
    }
}
