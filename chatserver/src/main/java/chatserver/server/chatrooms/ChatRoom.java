package chatserver.server.chatrooms;

import chatserver.server.clients.Client;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import lombok.Getter;
import lombok.NonNull;

import java.util.Objects;
import java.util.Set;

/**
 * ChatRoom of the chat server.
 */
public abstract class ChatRoom {

    @Getter
    protected Set<Client> clientsConnected;

    @Getter
    protected final String name;

    @Getter
    private final long referenceId;

    @Inject
    public ChatRoom(@Assisted String name, @Assisted long referenceId) {
        this.name = name;
        this.referenceId = referenceId;
    }

    /**
     * Adds a client as connected.
     * @param client the client to add
     */
    public void addClient(@NonNull Client client) {
        this.clientsConnected.add(client);
    }

    /**
     * Check if the chat room has clients connected.
     * @return true if is empty, false otherwise
     */
    public boolean isEmpty() {
        return getClientsConnected().isEmpty();
    }

    /**
     * Removes a client as connected.
     * @param client the client to be removed
     * @return true if the client was removed, false, otherwise
     */
    public boolean removeClient(@NonNull Client client) {
        return this.clientsConnected.remove(client);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
