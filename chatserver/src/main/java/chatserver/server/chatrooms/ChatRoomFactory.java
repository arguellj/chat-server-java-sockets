package chatserver.server.chatrooms;

/**
 * Factory used for Guice Dependency Injection Framework to create {@link ChatRoom}.
 */
public interface ChatRoomFactory {
    ChatRoom create(String name, long referenceId);
}
