package chatserver.server.clients;

import chatserver.readers.DataReader;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

@Log4j2
/**
 * Concrete {@link Client} for Asynchronous environments.
 */
public final class AsynchronousClient extends Client {
    private static final int BUFFER_SIZE = 4096; // 4 kb

    private AsynchronousSocketChannel channel;

    @Inject
    public AsynchronousClient(final DataReader reader, @Assisted final AsynchronousSocketChannel channel) {
        super(reader);
        this.channel = channel;
        this.chatRooms = ConcurrentHashMap.newKeySet();
    }

    @Override
    public void close() {
        if (this.channel.isOpen()) {
            try {
                this.channel.close();
            } catch (IOException e) {
                log.warn(e);
            }
        }
    }

    @Override
    public void read() throws ExecutionException, InterruptedException {
        ByteBuffer input = ByteBuffer.allocate(BUFFER_SIZE);

        if (this.channel.isOpen()) {
            final Integer result = this.channel.read(input).get();
            // Process asynchronously the message read, and come back to listen for incoming messages.
            CompletableFuture.runAsync(() -> this.reader.onData(this, input, result));
            read();
        }
    }

    @Override
    public void writeMessage(@NonNull final String message) throws ExecutionException, InterruptedException {
        this.writeMessage(ByteBuffer.wrap(message.getBytes()));
    }

    private synchronized void writeMessage(final ByteBuffer buffer) throws ExecutionException, InterruptedException {
        if (this.channel.isOpen()) {
            this.channel.write(buffer).get();
        }
        // TODO: if channel is closed, remove client because is disconnected.
    }
}