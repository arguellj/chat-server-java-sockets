package chatserver.server.clients;

import java.nio.channels.AsynchronousSocketChannel;

/***
 * Factory Used for Guice Dependency Injection Framework to create {@link Client} concrete classes.
 */
public interface ClientFactory {
    Client create(AsynchronousSocketChannel channel);
}
