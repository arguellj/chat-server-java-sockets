package chatserver.server.clients;

import chatserver.readers.DataReader;
import com.google.inject.Inject;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Client of the chat server.
 */
public abstract class Client {

    @Getter
    protected Set<Long> chatRooms;

    @Getter
    @Setter
    protected String name;

    @Getter
    @Setter
    protected long joinID;

    protected final DataReader reader;

    @Inject
    public Client(DataReader reader) {
        this.reader = reader;
    }

    /**
     * Closes the client's connection.
     */
    public abstract void close();

    /**
     * Read incoming messages from the client.
     */
    public abstract void read() throws ExecutionException, InterruptedException;

    /**
     * Writes a message to the client.
     *
     * @param string The message to be written
     */
    public abstract void writeMessage(final String string) throws ExecutionException, InterruptedException;
}
