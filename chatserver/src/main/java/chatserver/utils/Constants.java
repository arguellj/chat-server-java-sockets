package chatserver.utils;

/**
 * Constants for chat server
 */
public final class Constants {
    // Configuration Properties
    public static final int STUDENT_ID = 17307291;

    // Seeds
    public static final long CHAT_ROOM_REFERENCE_SEED_INITIAL_VALUE = 0;
    public static final long CLIENT_JOIN_ID_SEED_INITIAL_VALUE = 0;

    // Exception Codes
    public static final int INVALID_COMMAND_PARAMETERS_EXCEPTION_CODE = 0;
    public static final int INVALID_NOT_REGISTERED_CLIENT_EXCEPTION_CODE = 1;
    public static final int INVALID_CLIENT_NAME_EXCEPTION_CODE = 2;
    public static final int INVALID_CHAT_ROOM_EXCEPTION_CODE = 3;
    public static final int INVALID_JOIN_ID_EXCEPTION_CODE = 4;
    public static final int EXECUTION_COMMAND_EXCEPTION_CODE = 5;

    // Command Patterns
    public static final String COMMAND_PARAMETER_SEPARATOR = ":";
    public static final String MULTILINE_STRING_COMMAND_PARAMETER_PATTERN = COMMAND_PARAMETER_SEPARATOR + ".*(\r?\n){2}";
    public static final String STRING_COMMAND_PARAMETER_PATTERN =  COMMAND_PARAMETER_SEPARATOR + ".*\r?\n";
    public static final String NUMERIC_COMMAND_PARAMETER_PATTERN = COMMAND_PARAMETER_SEPARATOR + "\\s?\\d*\r?\n";
    public static final String HELO_COMMAND_PARAMETER_PATTERN = ".*\r?\n";

    public static final String HELLO_MESSAGE = "HELO";
    public static final String KILL_MESSAGE = "KILL_SERVICE";
    public static final String SERVER_METADATA =  "%s\nIP:%s\nPort:%d\nStudentID:%d\n";

    // Joining.
    public static final String JOIN_CHAT_ROOM_MESSAGE = "JOIN_CHATROOM";
    public static final String CLIENT_IP_MESSAGE = "CLIENT_IP";

    // Joined.
    public static final String JOINED_CHAT_ROOM_MESSAGE = "JOINED_CHATROOM";
    public static final String SERVER_IP_MESSAGE = "SERVER_IP";
    public static final String ROOM_REFERENCE_MESSAGE = "ROOM_REF";

    // Error.
    public static final String ERROR_CODE_MESSAGE = "ERROR_CODE";
    public static final String ERROR_DESCRIPTION_MESSAGE = "ERROR_DESCRIPTION";

    // Leaving.
    public static final String LEAVE_CHAT_ROOM_MESSAGE = "LEAVE_CHATROOM";

    // Left
    public static final String LEFT_CHAT_ROOM_MESSAGE = "LEFT_CHATROOM";

    // Disconnect.
    public static final String DISCONNECT_MESSAGE = "DISCONNECT";

    // Chat
    public static final String CHAT_REFERENCE_MESSAGE = "CHAT";
    public static final String CHAT_MESSAGE = "MESSAGE";

    // Common messages across different commands related to clients.
    public static final String CLIENT_NAME_MESSAGE = "CLIENT_NAME";
    public static final String JOIN_ID_MESSAGE = "JOIN_ID";

    // Common messages across different commands related to the chat server.
    public static final String PORT_MESSAGE = "PORT";

    private Constants() {
    }
}
