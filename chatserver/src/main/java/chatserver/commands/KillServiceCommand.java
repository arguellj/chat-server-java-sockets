package chatserver.commands;

import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;

import java.util.ArrayList;

import static chatserver.utils.Constants.KILL_MESSAGE;

/**
 * Kills the chat server
 */
public final class KillServiceCommand extends Command {

    @Inject
    public KillServiceCommand(final ChatServer chatServer) {
        super(chatServer, KILL_MESSAGE);
    }

    @Override
    protected String buildCommandPattern() {
        return "";
    }

    @Override
    protected String buildResponseFormat() {
        return "";
    }

    @Override
    protected void validate(Client client, ArrayList<String> parameterValues) {
    }

    @Override
    public void process(Client client, ArrayList<String> parameterValues) {
        this.chatServer.shutdown();
    }
}
