package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.ExecutionCommandException;
import chatserver.commands.exceptions.InvalidCommandParametersException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static chatserver.utils.Constants.COMMAND_PARAMETER_SEPARATOR;

@Log4j2
/**
 * A chat server command.
 */
public abstract class Command {

    protected final ChatServer chatServer;

    protected final String responseFormat;

    @Getter
    private static String commandIdentifier;

    private final String commandPattern;

    private int numberOfCommandParameters;

    @Inject
    public Command(ChatServer chatServer, String commandIdentifier) {
        this.chatServer = chatServer;
        this.commandIdentifier = commandIdentifier;
        this.commandPattern = this.buildCommandPattern();
        this.responseFormat = this.buildResponseFormat();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.commandIdentifier);
    }

    /**
     * Executes completely the command. It parses, validates and process the command and its parameters.
     * @param client the client who sent the command
     * @param message the client's message
     * @throws CommandException Abstract Exception that a Command can thrown
     */
    public void execute(@NonNull final Client client, @NonNull final String message) throws CommandException {
        log.debug(String.format("'%s' from Client id '%d' received.", this.getClass().getSimpleName(), client.getJoinID()));
        final ArrayList<String> parameterValues = this.processCommandParameters(message);

        if (parameterValues.size() != this.numberOfCommandParameters) {
            throw new InvalidCommandParametersException(
                    String.format("%s sent from %d does not have the exact number of parameters. Expected %d but received %d.",
                            this.getClass().getSimpleName(),
                            client.getJoinID(),
                            this.numberOfCommandParameters,
                            parameterValues.size()));
        }

        this.validate(client, parameterValues);
        this.process(client, parameterValues);

        log.debug(String.format("'%s' from Client id '%d' processed.", this.getClass().getSimpleName(), client.getJoinID()));
    }

    /**
     * Processes the command with the list of parameters {@param parameterValues}
     * @param client the client who sent the command
     * @param parameterValues the command parameters
     * @throws CommandException Abstract Exception that a Command can thrown
     */
    public abstract void process(@NonNull Client client, ArrayList<String> parameterValues) throws CommandException;

    /**
     * Gets the command Pattern of a parameter.
     * @param command the command parameter
     * @param pattern the pattern
     * @return the command Pattern of a parameter
     */
    protected String getCommandParameterPattern(@NonNull String command, @NonNull String pattern) {
        this.numberOfCommandParameters++;
        return  "(" + command + pattern + ")";
    }

    /**
     * Gets the response format used for sending response messages.
     * @param responseParameter the response parameter for a command parameter
     * @return the response format for a command parameter
     */
    protected String getResponseFormat(@NonNull String responseParameter) {
        return  responseParameter + COMMAND_PARAMETER_SEPARATOR + "%s" + System.getProperty("line.separator");
    }

    /**
     * Parses the message by using the command pattern
     * @param message message to parsed
     * @return the command parameters
     */
    protected ArrayList<String> processCommandParameters(@NonNull String message) {
        ArrayList<String> parameterValues = new ArrayList<>();
        Pattern pattern = Pattern.compile(this.commandPattern, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(message);

        if (matcher.find() &&
                (matcher.groupCount() == this.numberOfCommandParameters)) {
            // Start at index 1 because first (zero) group contains the complete message expected.
            for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
                String parameter = matcher.group(groupIndex);
                final int commandParameterSeparatorPosition = parameter.indexOf(COMMAND_PARAMETER_SEPARATOR);
                parameterValues.add(parameter.substring(commandParameterSeparatorPosition + 1).trim());
            }
        }

        return parameterValues;
    }

    /**
     * Sends a response directly to the client.
     * @param client the client to sent the message
     * @param values collection of string values that are going to be used as part of the message.
     * @throws CommandException Abstract Exception that a Command can thrown
     */
    protected void sendResponse(@NonNull final Client client, @NonNull final Object... values) throws CommandException {
        String message = String.format(this.responseFormat, values);
        try {
            log.debug(String.format("Response to client id '%d' with message '%s'.", client.getJoinID(), message));
            client.writeMessage(message);
        } catch (ExecutionException | InterruptedException e) {
           throw new ExecutionCommandException(
                   String.format("Response to client id '%d' could not be sent.", client.getJoinID()) , e);
        }
    }

    protected void setDummyCommandParameterPattern() {
        this.numberOfCommandParameters++;
    }

    /**
     * Build the command regular expression to be used to parse the command.
     * @return the command pattern
     */
    protected abstract String buildCommandPattern();

    /**
     * Builds the response format used in the response messages.
     * @return the response format
     */
    protected abstract String buildResponseFormat();

    /**
     * Validates the command parameters
     * @param client the client who sent the command
     * @param parameterValues he command parameters
     * @throws CommandException Abstract Exception that a Command can thrown
     */
    protected abstract void validate(@NonNull Client client, ArrayList<String> parameterValues) throws CommandException;
}
