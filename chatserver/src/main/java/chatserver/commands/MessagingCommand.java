package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidChatRoomException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.commands.exceptions.NotRegisteredClientException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;

import static chatserver.utils.Constants.*;

@Log4j2
/**
 * Command to send a broadcast message to a {@link chatserver.server.chatrooms.ChatRoom}
 */
public final class MessagingCommand extends Command {

    private static final int CHAT_REFERENCE_INDEX = 0;
    private static final int JOIN_ID_INDEX = 1;
    private static final int CLIENT_NAME_INDEX = 2;
    private static final int MESSAGE_INDEX = 3;

    @Inject
    public MessagingCommand(ChatServer chatServer) {
        super(chatServer, CHAT_REFERENCE_MESSAGE);
    }

    @Override
    protected String buildCommandPattern() {
        StringBuilder builder = new StringBuilder();

        builder.append(this.getCommandParameterPattern(CHAT_REFERENCE_MESSAGE, NUMERIC_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(JOIN_ID_MESSAGE, NUMERIC_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(CLIENT_NAME_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(CHAT_MESSAGE, MULTILINE_STRING_COMMAND_PARAMETER_PATTERN));
        this.setDummyCommandParameterPattern();

        return builder.toString();
    }

    @Override
    protected String buildResponseFormat() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getResponseFormat(CHAT_REFERENCE_MESSAGE));
        builder.append(this.getResponseFormat(CLIENT_NAME_MESSAGE));
        builder.append(this.getResponseFormat(CHAT_MESSAGE));

        return builder.toString();
    }

    @Override
    protected void validate(@NonNull Client client, @NonNull ArrayList<String> parameterValues) throws CommandException {
        if (client.getName() == null) {
            throw new NotRegisteredClientException(
                    String.format("Client with id '%d' does not have name registered.", client.getJoinID()));
        }

        if (!parameterValues.get(CLIENT_NAME_INDEX).equals(client.getName())) {
            throw new InvalidClientNameException(
                    String.format("Client with id '%d' was not registered with name '%s'.",
                            client.getJoinID(),
                            parameterValues.get(CLIENT_NAME_INDEX)));
        }

        // TODO: Uncomment when bug in client test be fixed.
        /**
        if (Long.valueOf(parameterValues.get(JOIN_ID_INDEX)) != client.getJoinID()) {
            throw new InvalidJoinIdException(
                    String.format("Invalid client id sent. Expected '%d' but received '%s'.",
                            client.getJoinID(),
                            parameterValues.get(JOIN_ID_INDEX)));
        }
         **/

        if (!client.getChatRooms().contains(Long.valueOf(parameterValues.get(CHAT_REFERENCE_INDEX)))) {
            throw new InvalidChatRoomException(
                    String.format("Invalid chat room reference sent. Expected '%s' but received '%s'.",
                            client.getChatRooms(),
                            parameterValues.get(CHAT_REFERENCE_INDEX)));
        }
    }

    @Override
    public void process(@NonNull Client client, @NonNull ArrayList<String> parameterValues) {
        String broadcastMessage = String.format(this.responseFormat,
                Long.valueOf(parameterValues.get(CHAT_REFERENCE_INDEX)),
                client.getName(),
                parameterValues.get(MESSAGE_INDEX) + System.getProperty("line.separator"));

        log.debug(String.format("Message to sent to chatroom '%s': '%s'", parameterValues.get(CHAT_REFERENCE_INDEX), broadcastMessage));
        this.chatServer.broadcastMessage(client, Long.valueOf(parameterValues.get(CHAT_REFERENCE_INDEX)), broadcastMessage);
    }
}
