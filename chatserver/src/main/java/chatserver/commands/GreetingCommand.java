package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;

import static chatserver.utils.Constants.*;

@Log4j2
/**
 * Command to replied the chat server identification information. Kind of ping.
 */
public final class GreetingCommand extends Command {

    private static final int HELO_MESSAGE_INDEX = 0;

    @Inject
    public GreetingCommand(final ChatServer chatServer) {
        super(chatServer, HELLO_MESSAGE);
    }

    @Override
    protected String buildCommandPattern() {
        StringBuilder builder = new StringBuilder();
        builder.append(getCommandParameterPattern(HELLO_MESSAGE, HELO_COMMAND_PARAMETER_PATTERN));

        return builder.toString();
    }

    @Override
    protected String buildResponseFormat() {
        return SERVER_METADATA;
    }

    @Override
    protected void validate(Client client, ArrayList<String> parameterValues) {
    }

    @Override
    public void process(@NonNull Client client, @NonNull ArrayList<String> parameterValues) throws CommandException {
        log.debug(String.format("Sending Greeting message to Client id '%d'.", client.getJoinID()));
        this.sendResponse(client,
                parameterValues.get(HELO_MESSAGE_INDEX),
                this.chatServer.getIpAddress(), this.chatServer.getPort(), this.chatServer.getStudentId());

        log.info(String.format("Sent Greeting message to Client id '%d'.", client.getJoinID()));
    }

}
