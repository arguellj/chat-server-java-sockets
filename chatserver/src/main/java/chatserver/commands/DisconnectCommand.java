package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.commands.exceptions.NotRegisteredClientException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import static chatserver.utils.Constants.*;

@Log4j2
/**
 * Command for disconnecting a {@link Client} from the chat server.
 */
public final class DisconnectCommand extends Command {

    private static final int CLIENT_NAME_INDEX = 2;

    private static Command commandHelper;

    @Inject
    public DisconnectCommand(ChatServer chatServer) {
        super(chatServer, DISCONNECT_MESSAGE);
        this.commandHelper = new MessagingCommand(chatServer);
    }

    @Override
    protected String buildCommandPattern() {
        final StringBuilder builder = new StringBuilder();
        builder.append(this.getCommandParameterPattern(DISCONNECT_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(PORT_MESSAGE, NUMERIC_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(CLIENT_NAME_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));

        return builder.toString();
    }

    @Override
    protected String buildResponseFormat() {
        return null;
    }

    @Override
    protected void validate(@NonNull Client client, @NonNull ArrayList<String> parameterValues) throws CommandException {
        if (client.getName() == null) {
            throw new NotRegisteredClientException(
                    String.format("Client with id '%d' does not have name registered.", client.getJoinID()));
        }

        if (!parameterValues.get(CLIENT_NAME_INDEX).equals(client.getName())) {
            throw new InvalidClientNameException(
                    String.format("Client with id '%d' was not registered with name '%s'.",
                            client.getJoinID(),
                            parameterValues.get(CLIENT_NAME_INDEX)));
        }
    }

    @Override
    public void process(@NonNull Client client, ArrayList<String> parameterValues) {
        final Set<Long> chatRoomsDisconnected = client.getChatRooms();

        chatRoomsDisconnected
                .parallelStream()
                .map((Long chatRoomReferenceId) -> String.valueOf(chatRoomReferenceId))
                .forEach((String chatRoomReferenceId) -> {
                    String broadcastMessage =
                            String.format("'%s' has left the chatroom '%s.",
                                    client.getName(), chatRoomReferenceId);

                    ArrayList<String> chatCommandParameters = new ArrayList<>(Arrays.asList(
                            chatRoomReferenceId,
                            String.valueOf(client.getJoinID()),
                            client.getName(),
                            broadcastMessage));

                    log.debug(String.format("Sending disconnect message to Clients in chatroom '%s'.", chatRoomReferenceId));

                    try {
                        this.commandHelper.process(client, chatCommandParameters);
                    } catch (CommandException e) {
                        log.warn(e);
                    }
                });

        this.chatServer.disconnectClient(client);
        log.info(String.format("Client id '%d' has disconnected.", client.getJoinID()));
    }
}
