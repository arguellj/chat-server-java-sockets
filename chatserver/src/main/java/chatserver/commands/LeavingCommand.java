package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.commands.exceptions.InvalidJoinIdException;
import chatserver.commands.exceptions.NotRegisteredClientException;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;

import static chatserver.utils.Constants.*;

@Log4j2
/**
 * Command for leaving a {@link chatserver.server.chatrooms.ChatRoom}
 */
public final class LeavingCommand extends Command {

    private static final int LEAVE_CHAT_ROOM_INDEX = 0;
    private static final int JOIN_ID_INDEX = 1;
    private static final int CLIENT_NAME_INDEX = 2;

    private static Command commandHelper;

    @Inject
    public LeavingCommand(final ChatServer chatServer) {
        super(chatServer, LEAVE_CHAT_ROOM_MESSAGE);
        this.commandHelper = new MessagingCommand(chatServer);
    }

    @Override
    protected String buildCommandPattern() {
        StringBuilder builder = new StringBuilder();

        builder.append(this.getCommandParameterPattern(LEAVE_CHAT_ROOM_MESSAGE, NUMERIC_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(JOIN_ID_MESSAGE, NUMERIC_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(CLIENT_NAME_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));

        return builder.toString();
    }

    @Override
    protected String buildResponseFormat() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getResponseFormat(LEFT_CHAT_ROOM_MESSAGE));
        builder.append(this.getResponseFormat(JOIN_ID_MESSAGE));

        return builder.toString();
    }

    @Override
    protected void validate(@NonNull Client client, @NonNull ArrayList<String> parameterValues) throws CommandException {

        if (client.getName() == null) {
            throw new NotRegisteredClientException(
                    String.format("Client with id '%d' does not have name registered.", client.getJoinID()));
        }

        if (!parameterValues.get(CLIENT_NAME_INDEX).equals(client.getName())) {
            throw new InvalidClientNameException(
                    String.format("Client with id '%d' was not registered with name '%s'.",
                            client.getJoinID(),
                            parameterValues.get(CLIENT_NAME_INDEX)));
        }

        if (Long.valueOf(parameterValues.get(JOIN_ID_INDEX)) != client.getJoinID()) {
            throw new InvalidJoinIdException(
                    String.format("Invalid client id sent. Expected '%d' but received '%s'.",
                            client.getJoinID(),
                            parameterValues.get(JOIN_ID_INDEX)));
        }

    }

    @Override
    public void process(@NonNull Client client, @NonNull ArrayList<String> parameterValues) throws CommandException {
        String chatRoomReferenceId = parameterValues.get(LEAVE_CHAT_ROOM_INDEX);

        log.debug(String.format("Sending left message to Client id '%d'.", client.getJoinID()));
        this.sendResponse(client, chatRoomReferenceId, client.getJoinID());

        String broadcastMessage =
                String.format("'%s' has left the chatroom '%s'.",
                        client.getName(), parameterValues.get(LEAVE_CHAT_ROOM_INDEX));

        ArrayList<String> chatCommandParameters = new ArrayList<>(Arrays.asList(
                parameterValues.get(LEAVE_CHAT_ROOM_INDEX),
                String.valueOf(client.getJoinID()),
                client.getName(),
                broadcastMessage));

        log.debug(String.format("Sending left message to Clients in chatroom '%s'.", chatRoomReferenceId));
        this.commandHelper.process(client, chatCommandParameters);


        this.chatServer.leaveClient(client, Long.valueOf(chatRoomReferenceId));
        log.info(String.format("Client id '%d' has left chatroom '%s'.", client.getJoinID(), chatRoomReferenceId));
    }
}
