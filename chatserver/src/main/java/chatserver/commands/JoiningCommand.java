package chatserver.commands;

import chatserver.commands.exceptions.CommandException;
import chatserver.commands.exceptions.InvalidClientNameException;
import chatserver.server.chatrooms.ChatRoom;
import chatserver.server.ChatServer;
import chatserver.server.clients.Client;
import com.google.inject.Inject;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;

import static chatserver.utils.Constants.*;

@Log4j2
/**
 * Command for joining a {@link Client} to a {@link ChatRoom}
 */
public final class JoiningCommand extends Command {

    private static final int CHAT_ROOM_NAME_INDEX = 0;
    private static final int CLIENT_NAME_INDEX = 3;

    private static Command commandHelper;

    @Inject
    public JoiningCommand(final ChatServer chatServer) {
        super(chatServer, JOIN_CHAT_ROOM_MESSAGE);
        this.commandHelper = new MessagingCommand(chatServer);
    }

    @Override
    protected String buildCommandPattern() {
        final StringBuilder builder = new StringBuilder();
        builder.append(this.getCommandParameterPattern(JOIN_CHAT_ROOM_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(CLIENT_IP_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(PORT_MESSAGE, NUMERIC_COMMAND_PARAMETER_PATTERN));
        builder.append(this.getCommandParameterPattern(CLIENT_NAME_MESSAGE, STRING_COMMAND_PARAMETER_PATTERN));

        return builder.toString();
    }

    @Override
    protected String buildResponseFormat() {
        final StringBuilder builder = new StringBuilder();
        builder.append(this.getResponseFormat(JOINED_CHAT_ROOM_MESSAGE));
        builder.append(this.getResponseFormat(SERVER_IP_MESSAGE));
        builder.append(this.getResponseFormat(PORT_MESSAGE));
        builder.append(this.getResponseFormat(ROOM_REFERENCE_MESSAGE));
        builder.append(this.getResponseFormat(JOIN_ID_MESSAGE));

        return  builder.toString();
    }

    @Override
    protected void validate(@NonNull Client client, @NonNull ArrayList<String> parameterValues)
            throws CommandException {
        if (client.getName() == null) {
            client.setName(parameterValues.get(CLIENT_NAME_INDEX));
        } else if (!client.getName().equals(parameterValues.get(CLIENT_NAME_INDEX))) {
            throw new InvalidClientNameException(
                    String.format("Client with id '%d' was not registered with name '%s'.",
                            client.getJoinID(),
                            parameterValues.get(CLIENT_NAME_INDEX)));
        }
    }

    @Override
    public void process(@NonNull Client client, @NonNull ArrayList<String> parameterValues) throws CommandException {
        final ChatRoom chatRoom = this.chatServer.getOrCreateChatRoom(parameterValues.get(CHAT_ROOM_NAME_INDEX));

        if (client.getJoinID() == CLIENT_JOIN_ID_SEED_INITIAL_VALUE) {
            client.setJoinID(this.chatServer.getNextJoinId());
        }

        log.debug(String.format("Sending joined message to Client id '%d'.", client.getJoinID()));
        this.sendResponse(client,
                parameterValues.get(CHAT_ROOM_NAME_INDEX),
                this.chatServer.getIpAddress(),
                this.chatServer.getPort(),
                chatRoom.getReferenceId(),
                client.getJoinID());

        chatServer.joinClient(client, chatRoom.getReferenceId());

        String broadcastMessage = String.format("'%s' has joined to chatroom '%s'.",
                client.getName(), parameterValues.get(CHAT_ROOM_NAME_INDEX));

        ArrayList<String> chatCommandParameters =
                new ArrayList<>(Arrays.asList(
                        String.valueOf(chatRoom.getReferenceId()),
                        String.valueOf(client.getJoinID()),
                        client.getName(),
                        broadcastMessage));

        log.debug(String.format("Sending joined message to Clients in chatroom '%s'.", chatRoom.getReferenceId()));
        this.commandHelper.process(client, chatCommandParameters);

        log.info(String.format("Client id '%d' has joined chatroom '%s'.", client.getJoinID(), chatRoom.getReferenceId()));
    }
}
