package chatserver.commands.exceptions;

import static chatserver.utils.Constants.INVALID_JOIN_ID_EXCEPTION_CODE;

public final class InvalidJoinIdException extends CommandException {
    public InvalidJoinIdException(String message) {
        super(message, INVALID_JOIN_ID_EXCEPTION_CODE);
    }
}
