package chatserver.commands.exceptions;

import static chatserver.utils.Constants.INVALID_CHAT_ROOM_EXCEPTION_CODE;

/**
 * Validation exception used to detect invalid chat rooms reference.
 */
public final class InvalidChatRoomException extends CommandException {
    public InvalidChatRoomException(String message) {
        super(message, INVALID_CHAT_ROOM_EXCEPTION_CODE);
    }
}
