package chatserver.commands.exceptions;

import static chatserver.utils.Constants.EXECUTION_COMMAND_EXCEPTION_CODE;

/**
 * Exception that occurs when unexpected error happens during the execution of a command.
 */
public class ExecutionCommandException extends CommandException {

    public ExecutionCommandException(String message) {
        super(message, EXECUTION_COMMAND_EXCEPTION_CODE);
    }

    public ExecutionCommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
