package chatserver.commands.exceptions;

import lombok.Getter;

/**
 * Exception of Commands
 */
public abstract class CommandException extends Exception {

    @Getter
    private int code;

    public CommandException(String message, int code) {
        super(message);
        this.code = code;
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }
}