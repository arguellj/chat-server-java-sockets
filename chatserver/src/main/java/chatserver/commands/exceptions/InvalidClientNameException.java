package chatserver.commands.exceptions;

import static chatserver.utils.Constants.INVALID_CLIENT_NAME_EXCEPTION_CODE;

/**
 * Validation exception used to detect invalid client reference.
 */
public final class InvalidClientNameException extends CommandException {
    public InvalidClientNameException(String message) {
        super(message, INVALID_CLIENT_NAME_EXCEPTION_CODE);
    }
}
