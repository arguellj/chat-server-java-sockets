package chatserver.commands.exceptions;

import static chatserver.utils.Constants.INVALID_COMMAND_PARAMETERS_EXCEPTION_CODE;

/**
 * Validation exception used to detect invalid command parameters.
 */
public final class InvalidCommandParametersException extends CommandException {
    public InvalidCommandParametersException(String message) {
        super(message, INVALID_COMMAND_PARAMETERS_EXCEPTION_CODE);
    }
}
