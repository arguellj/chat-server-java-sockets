package chatserver.commands.exceptions;

import static chatserver.utils.Constants.INVALID_NOT_REGISTERED_CLIENT_EXCEPTION_CODE;

/**
 * Validation exception used to detect when a client not registered is tryng to do a action that requires to be registered.
 */
public final class NotRegisteredClientException extends CommandException {
    public NotRegisteredClientException(String message) {
        super(message, INVALID_NOT_REGISTERED_CLIENT_EXCEPTION_CODE);
    }
}
