package chatserver;

import chatserver.commands.*;
import chatserver.readers.ClientReader;
import chatserver.readers.DataReader;
import chatserver.server.AsynchronousChatServer;
import chatserver.server.ChatServer;
import chatserver.server.chatrooms.AsynchronousChatRoom;
import chatserver.server.chatrooms.ChatRoom;
import chatserver.server.chatrooms.ChatRoomFactory;
import chatserver.server.clients.AsynchronousClient;
import chatserver.server.clients.Client;
import chatserver.server.clients.ClientFactory;
import chatserver.sockets.AsynchronousServerSocket;
import chatserver.sockets.AsynchronousSocket;
import chatserver.sockets.Socket;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;
import lombok.extern.log4j.Log4j2;

import static chatserver.utils.Constants.*;

@Log4j2
/**
 * Bootstrap module used for Guice Dependency Injection Framework to bind Interfaces and Abstract clasess to Concrete classes.
 */
public class ChatServerModule extends AbstractModule {
    @Override
    protected void configure() {
        this.configureCommands();

        bind(Socket.class).to(AsynchronousSocket.class);
        bind(AsynchronousSocket.class).to(AsynchronousServerSocket.class);
        bind(ChatServer.class).to(AsynchronousChatServer.class);
        bind(DataReader.class).to(ClientReader.class);

        this.createFactories();
    }

    private void configureCommands() {
        MapBinder<String, Command> mapBinder = MapBinder.newMapBinder(binder(), String.class, Command.class);
        mapBinder.addBinding(DISCONNECT_MESSAGE).to(DisconnectCommand.class);
        mapBinder.addBinding(HELLO_MESSAGE).to(GreetingCommand.class);
        mapBinder.addBinding(JOIN_CHAT_ROOM_MESSAGE).to(JoiningCommand.class);
        mapBinder.addBinding(KILL_MESSAGE).to(KillServiceCommand.class);
        mapBinder.addBinding(LEAVE_CHAT_ROOM_MESSAGE).to(LeavingCommand.class);
        mapBinder.addBinding(CHAT_REFERENCE_MESSAGE).to(MessagingCommand.class);
    }

    private void createFactories() {
        install(new FactoryModuleBuilder()
                .implement(ChatRoom.class, AsynchronousChatRoom.class)
                .build(ChatRoomFactory.class));
        install(new FactoryModuleBuilder()
                .implement(Client.class, AsynchronousClient.class)
                .build(ClientFactory.class));
    }
}